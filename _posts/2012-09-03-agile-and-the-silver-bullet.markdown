---
layout: post
title: Agile Software Development
published: false
categories:
---

Agile is prideful. It strokes the egos of the most egotistical group in software development: developers. It promotes the over-reliance and heroism of developers. It demotes managers in the organization to "overhead" and pushes other important roles such as quality analysts to the wayside. Agile is not the silver bullet to delivering great software faster than everyone else.

## The Motivation of People

In his book Drive, Daniel Pink uses the Agile software movement as an example of the new way of business. In his new way of business, people are not motivated by survival (eat and sleep) or by external motivators (sell 20 pens, get a cash bonus). People are motivated by intrinsic motivation. They are motivated to do well in order to do well.

It feels right. People that do best do seem to be motivated for their own reasons instead of reasons dictated to them by their superiors.

**Axiom 1: Intrinsically motivated people develop better software.**

I believe that this statement is true; if you disagree with it I would definitely recommend reading Pink's Drive along with the numerous references included in that book.

## Agile and Developers

Agile is a software methodology that puts the creators, the developers, in the heart of the process. The world revolves around developers. I cannot tell you how many times in my day to day work I have heard phrases along the lines of: "How can we remove blockers for the devs?", "Can we get stories to devs faster?", "If we move more testing automation to the QA team, we can speed up the developers.", etc.

Agile was created by developers for developers. Part of the ethos of Agile is to upend the notion that managers are in control and to give the Makers and Doers the control and power. 
For developers that love to create for the sake of creating, to improve for the sake of improvement, to make the best software possible because it is about the process and less about the reward, we find that Agile caters to them.

**Axiom 2: Agile unleashes the intrinsically motivated developers to create good software.**

We all know the proposed benefits of Agile: you develop better software faster, you develop the right features because the feedback loop is smaller, you create software that is easier to change and more maintainable, you have a happier team that does not burn out as quickly, etc.

