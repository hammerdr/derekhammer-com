--- 
wordpress_id: 40
layout: post
title: Personal Source Control
wordpress_url: http://blog.derekhammer.com/?p=40
---
Today, I was finishing what going to be called Yet Another Calculator v1.0 (more on YAC in another post). I realized that I had failed to use source control during the project. This isn't disastrous. I didn't ever lose any data. My builds all work for my intended purpose. This, unlike most projects, didn't really have an intended audience but was, instead, me hacking code.

However, I still think the project should be source controlled. What if I'm inspecting a particular section of code and I wonder "Why did I do that?" Instead of guessing, the context of when and with what other changes and check in comments should help me decipher why I made a particular decision.

<!--more-->

So, why didn't I do it? As Jeff Atwood often says, <a href="http://www.codinghorror.com/blog/archives/000940.html" target="_blank">users should fall into the pit of success</a>. What this means is that, if using source control is a general "success" criterion, then I should have to <strong>try</strong> to not use it. Instead, source control (and software configuration management, in general) is always difficult to set up and maintain. Should software development suites integrate source control automatically?

In part, this is the fault of Visual Studio, Eclipse, Xcode, etc. However, really I think this problem affects the whole configuration management community. It is pretty <strong>awkward</strong> to set up Git. It is pretty <strong>expensive</strong> to set up Team Foundation or Clearcase. SVN is the easiest of these tools to setup and use (and, really, <a href="http://code.google.com">only because of Google</a>) and it still takes some conscious effort (it is entirely possible to have SVN set up and only commit a project once.. without having to make an effort to not commit).

Admittedly, this is usually solved by teaching programmers and engineers the importance of source control. They understand because it saves them from losing their data. However, it still takes effort that really shouldn't be necessary.

And, beyond the domain of programming, document control is nonexistent. Sure, people will regularly back up data, if they know what they are doing. However, this is a process that is both insufficient and difficult. Instead, the operating system should act as a document management system.

With virtually unlimited storage space available to computers, shouldn't document revision histories be kept? This is the sort of automated process that computers can handle without the user worrying about it. If there is a fatal system crash, documents can be <em>automatically recovered</em>. If there is a discrepancy in the files for some reason, change histories could be automatically display to the user at their perusing.

For editing collaborative works, such as <strong>projects</strong> or large research papers, revision history could be everything. This could be automatically handled without setup or conscious effort. In short, the user would fall into the pit of success. Obviously, more complex and in-depth systems are needed for source control, specifically, and that is a part of developing software. But, instead of making it something that developers need to fight, we should make source control easy and as transparent as possible.
