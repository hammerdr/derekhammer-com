---
layout: post
title: A Pattern for Rails Presenters
published: true
categories:
---
On my previous project, my team developed a pattern for Rails presenters that I think is worth sharing. We used this pattern to a great deal of success and I will be advocating for its use in future Rails projects.

The Presenter pattern is what I call a projection of a model. That is: given a model A which contains data Z, a presenter is a derivation of model A and data Z. We can represent this as A' (Presenter class) and Z' (Presenter data). For any given model A, there are an unlimited number of projections and, thus, presenters.

Let's do something more concrete: we have an object of class User with data <code>{first_name: 'Derek', last_name: 'Hammer', gender: 'M'}</code>. A presenter might be called UserSalutations and is a derivation of the User model. The data might be <code>{greeting: 'Hello, Mr. D. Hammer'}</code>.

The benefits of the Presenter pattern is to separate domain logic (the changing of a person's name) and the presentation logic (formatting the person's name as a salutation). I will be quick to point out that the pattern is not always applicable and should be used as a conscious choice rather than as a matter of course.

We have User and we have UserSalutation. Both of these are a part of our system. How might the code that uses this look?

<div class="highlight"><pre><code class="ruby"><span class="k">class</span> <span class="nc">MessagesController</span> <span class="o">&lt;</span> <span class="no">ApplicationController</span>
    <span class="k">def</span> <span class="nf">new</span>
        <span class="vi">@message</span> <span class="o">=</span> <span class="no">Message</span><span class="o">.</span><span class="n">new</span>
        <span class="vi">@user</span> <span class="o">=</span> <span class="no">User</span><span class="o">.</span><span class="n">find</span><span class="p">(</span><span class="n">params</span><span class="o">[</span><span class="ss">:id</span><span class="o">]</span><span class="p">)</span>
        <span class="vi">@salutations</span> <span class="o">=</span> <span class="no">UserSalutations</span><span class="o">.</span><span class="n">new</span><span class="p">(</span><span class="vi">@user</span><span class="o">.</span><span class="n">first_name</span><span class="p">,</span> <span class="vi">@user</span><span class="o">.</span><span class="n">last_name</span><span class="p">,</span> <span class="vi">@user</span><span class="o">.</span><span class="n">gender</span><span class="p">,</span> <span class="vi">@user</span><span class="o">.</span><span class="n">id</span><span class="p">)</span>
    <span class="k">end</span>
<span class="k">end</span>

<span class="k">class</span> <span class="nc">UserSalutations</span>
    <span class="kp">attr_reader</span> <span class="ss">:last_name</span><span class="p">,</span> <span class="ss">:gender</span><span class="p">,</span> <span class="ss">:first_name</span><span class="p">,</span> <span class="ss">:user_id</span>

    <span class="k">def</span> <span class="nf">initialize</span> <span class="n">first_name</span><span class="p">,</span> <span class="n">last_name</span><span class="p">,</span> <span class="n">gender</span><span class="p">,</span> <span class="n">user_id</span>
        <span class="vi">@first_name</span> <span class="o">=</span> <span class="n">first_name</span>
        <span class="vi">@last_name</span> <span class="o">=</span> <span class="n">last_name</span>
        <span class="vi">@gender</span> <span class="o">=</span> <span class="n">gender</span>
        <span class="vi">@user_id</span> <span class="o">=</span> <span class="n">user_id</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nf">greeting</span>
        <span class="s2">"Hello, </span><span class="si">#{</span><span class="n">honorific</span><span class="si">}</span><span class="s2"> </span><span class="si">#{</span><span class="n">first_initial</span><span class="si">}</span><span class="s2">. </span><span class="si">#{</span><span class="n">last_name</span><span class="si">}</span><span class="s2">"</span>
    <span class="k">end</span>

    <span class="kp">private</span>

    <span class="k">def</span> <span class="nf">honorific</span>
        <span class="k">case</span> <span class="n">gender</span>
        <span class="k">when</span> <span class="s1">'M'</span> <span class="k">then</span> <span class="s1">'Mr.'</span>
        <span class="k">when</span> <span class="s1">'F'</span> <span class="k">then</span> <span class="n">ms_or_mrs</span>
        <span class="k">else</span> <span class="k">then</span> <span class="s1">'Mr.'</span>
        <span class="k">end</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nf">first_initial</span>
        <span class="n">first_name</span><span class="o">.</span><span class="n">first</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nf">ms_or_mrs</span>
        <span class="k">if</span> <span class="no">Marriage</span><span class="o">.</span><span class="n">where</span><span class="p">(</span><span class="n">person_one_id</span><span class="p">:</span> <span class="n">user_id</span><span class="p">)</span><span class="o">.</span><span class="n">or</span><span class="o">.</span><span class="n">where</span><span class="p">(</span><span class="n">person_two_id</span><span class="p">:</span> <span class="n">user_id</span><span class="p">)</span><span class="o">.</span><span class="n">any?</span><span class="p">(</span><span class="o">&amp;</span><span class="ss">:active?</span><span class="p">)</span>
            <span class="s2">"Mrs."</span>
        <span class="k">else</span>
            <span class="s2">"Ms."</span>
        <span class="k">end</span>
    <span class="k">end</span>
<span class="k">end</span>
</code></pre>
</div>

This method works, but we are doing some database access in our presenter. This makes the presenter difficult to test and require outside dependencies. One solution is to push the code "up" into the controller.

<div class="highlight"><pre><code class="ruby"><span class="k">class</span> <span class="nc">MessagesController</span> <span class="o">&lt;</span> <span class="no">ApplicationController</span>
    <span class="k">def</span> <span class="nf">new</span>
        <span class="vi">@message</span> <span class="o">=</span> <span class="no">Message</span><span class="o">.</span><span class="n">new</span>
        <span class="vi">@user</span> <span class="o">=</span> <span class="no">User</span><span class="o">.</span><span class="n">find</span><span class="p">(</span><span class="n">params</span><span class="o">[</span><span class="ss">:id</span><span class="o">]</span><span class="p">)</span>
        <span class="vi">@salutations</span> <span class="o">=</span> <span class="no">UserSalutations</span><span class="o">.</span><span class="n">new</span><span class="p">(</span><span class="vi">@user</span><span class="o">.</span><span class="n">first_name</span><span class="p">,</span> <span class="vi">@user</span><span class="o">.</span><span class="n">last_name</span><span class="p">,</span> <span class="n">honorific</span><span class="p">)</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nf">honorific</span>
        <span class="k">case</span> <span class="n">gender</span>
        <span class="k">when</span> <span class="s1">'M'</span> <span class="k">then</span> <span class="s1">'Mr.'</span>
        <span class="k">when</span> <span class="s1">'F'</span> <span class="k">then</span> <span class="n">ms_or_mrs</span>
        <span class="k">else</span> <span class="k">then</span> <span class="s1">'Mr.'</span>
        <span class="k">end</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nf">ms_or_mrs</span>
        <span class="k">if</span> <span class="no">Marriage</span><span class="o">.</span><span class="n">where</span><span class="p">(</span><span class="n">person_one_id</span><span class="p">:</span> <span class="n">user_id</span><span class="p">)</span><span class="o">.</span><span class="n">or</span><span class="o">.</span><span class="n">where</span><span class="p">(</span><span class="n">person_two_id</span><span class="p">:</span> <span class="n">user_id</span><span class="p">)</span><span class="o">.</span><span class="n">any?</span><span class="p">(</span><span class="o">&amp;</span><span class="ss">:active?</span><span class="p">)</span>
            <span class="s2">"Mrs."</span>
        <span class="k">else</span>
            <span class="s2">"Ms."</span>
        <span class="k">end</span>
    <span class="k">end</span>
<span class="k">end</span>
</code></pre>
</div>


<p>This solution is actually worse, though! We are creating a fat controller and controllers are notoriously difficult to test and maintain (hence the skinny controllers mantra espoused by the Rails community). The solution that my team came up with was the following:</p>

<div class="highlight"><pre><code class="ruby"><span class="k">class</span> <span class="nc">MessagesController</span> <span class="o">&lt;</span> <span class="no">ApplicationController</span>
    <span class="k">def</span> <span class="nf">new</span>
        <span class="vi">@message</span> <span class="o">=</span> <span class="no">Message</span><span class="o">.</span><span class="n">new</span>
        <span class="vi">@user</span> <span class="o">=</span> <span class="no">User</span><span class="o">.</span><span class="n">find</span><span class="p">(</span><span class="n">params</span><span class="o">[</span><span class="ss">:id</span><span class="o">]</span><span class="p">)</span>
        <span class="vi">@salutations</span> <span class="o">=</span> <span class="no">UserSalutations</span><span class="o">.</span><span class="n">from_user</span><span class="p">(</span><span class="vi">@user</span><span class="p">)</span>
    <span class="k">end</span>
<span class="k">end</span>


<span class="k">class</span> <span class="nc">UserSalutations</span>
    <span class="kp">attr_reader</span> <span class="ss">:last_name</span><span class="p">,</span> <span class="ss">:first_name</span><span class="p">,</span> <span class="ss">:honorific</span>
    
    <span class="k">def</span> <span class="nc">self</span><span class="o">.</span><span class="nf">from_user</span> <span class="n">user</span>
        <span class="nb">self</span><span class="o">.</span><span class="n">new</span> <span class="n">user</span><span class="o">.</span><span class="n">first_name</span><span class="p">,</span> <span class="n">user</span><span class="o">.</span><span class="n">last_name</span><span class="p">,</span> <span class="n">honorific</span><span class="p">(</span><span class="n">user</span><span class="p">)</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nc">self</span><span class="o">.</span><span class="nf">honorific</span> <span class="n">user</span>
        <span class="k">case</span> <span class="n">user</span><span class="o">.</span><span class="n">gender</span>
        <span class="k">when</span> <span class="s1">'M'</span> <span class="k">then</span> <span class="s1">'Mr.'</span>
        <span class="k">when</span> <span class="s1">'F'</span> <span class="k">then</span> <span class="n">ms_or_mrs</span><span class="p">(</span><span class="n">user</span><span class="p">)</span>
        <span class="k">else</span> <span class="k">then</span> <span class="s1">'Mr.'</span>
        <span class="k">end</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nc">self</span><span class="o">.</span><span class="nf">ms_or_mrs</span> <span class="n">user</span>
        <span class="k">if</span> <span class="no">Marriage</span><span class="o">.</span><span class="n">where</span><span class="p">(</span><span class="n">person_one_id</span><span class="p">:</span> <span class="n">user_id</span><span class="p">)</span><span class="o">.</span><span class="n">or</span><span class="o">.</span><span class="n">where</span><span class="p">(</span><span class="n">person_two_id</span><span class="p">:</span> <span class="n">user_id</span><span class="p">)</span><span class="o">.</span><span class="n">any?</span><span class="p">(</span><span class="o">&amp;</span><span class="ss">:active?</span><span class="p">)</span>
            <span class="s2">"Mrs."</span>
        <span class="k">else</span>
            <span class="s2">"Ms."</span>
        <span class="k">end</span>
    <span class="k">end</span>

    <span class="k">def</span> <span class="nf">initialize</span> <span class="n">first_name</span><span class="p">,</span> <span class="n">last_name</span><span class="p">,</span> <span class="n">honorific</span>
        <span class="vi">@first_name</span> <span class="o">=</span> <span class="n">first_name</span>
        <span class="vi">@last_name</span> <span class="o">=</span> <span class="n">last_name</span>
        <span class="vi">@honorific</span> <span class="o">=</span> <span class="n">honorific</span>
    <span class="k">end</span>

    <span class="err">…</span>
<span class="k">end</span>
</code></pre>
</div>


Here, we have a testable, dependency free UserSalutations object. The dependencies are encapsulated by the factory method on the UserSalutations class object. Our Rails controller is still skinny (actually, its skinnier than before!). The downside is that the factory methods need to talk to external dependencies. This is okay, I think, because we are reducing the complexity here down to its bare minimum and testing in isolation.

So, in short, use a combination of factory methods and PROs in order to keep your presenters testable, maintainable and your controllers skinny!
