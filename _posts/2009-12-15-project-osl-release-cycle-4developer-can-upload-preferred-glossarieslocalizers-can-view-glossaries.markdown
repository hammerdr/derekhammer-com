--- 
wordpress_id: 143
layout: post
title: "Project OSL :: Release Cycle 4"
wordpress_url: http://blog.derekhammer.com/?p=143
---
<p style="clear: both">It's been a while. Thanksgiving break and a slow start led us to have a couple of weeks without a release. But, we released a big one. This is easily our largest release in terms of changes to both the code and the users' experience.</p>
<p style="clear: both">Here are the two features that we implemented:</p>

<ul style="clear: both">
	<li>Developer can upload preferred glossaries</li>
	<li>Localizers can view glossaries</li>
</ul>
The glossaries were the first feature that we added that existed off of the main workflow. Given our architecture and practice of refactoring, this was surprisingly easy to implement.
<div>We also fixed some bugs:</div>
<div>
<ul style="clear: both">
	<li>Resources context is not preserved (context refers to the location of the resource in the application)</li>
	<li>Multiple uploads per session is not allowed</li>
	<li>Voting isn't tied to users and users can upload multiple times.</li>
	<li>There was no feedback whether uploading worked or not.</li>
	<li>Context for localizable strings is lost on importing (context refers to comments on key-value pairs in .strings files)</li>
</ul>
</div>
<div>And we also folded some user feedback into our system:</div>
<div>
<ul style="clear: both">
	<li>Are votes supposed to only apply to each line item, or to the file as a whole? I think that might be something to tackle in the next RC. I know that was something we said really depended on the user model being implemented, or at least started. (Caleb Allen)</li>
	<li>The submit feedback form should remember the last used email address. Iíve had to enter it each time (even w/in the same session). (Tim Wood)</li>
	<li>Iím already ìlogged inî, can you pre-populate the email field in the feedback window with my email? (Caleb Allen)</li>
</ul>
</div>
<div>The big underlying problem that solved many of the bugs and user requests was a project metaphor. In RC3 we allowed users to upload a specific .strings file. We would then parse the file and display that file back to the user. The project metaphor extends and encapsulates this idea. Now, OSL has projects that represent the applications that you have uploaded. Each of these projects has a litany of resources that the users can browse. So, instead of uploading resource files, users now upload .app files and we automatically parse the entire application.</div>
<h1><strong>Refactoring</strong></h1>
<div>We refactored a lot. We probably refactored too much. Chandler and I went crazy on the refactorings for this release cycle. Much of it was necessary in order to make the project metaphor fit into our application nicely. Some of it was just Chandler and I going a little overboard. Overall, it was a great time. There are a few very ugly parts of our code and we cringe every time we look at it, but most of that exists in view code. The rest of it will just need to be looked at in the next release cycle.</div>
<h1><strong>Atlas and Cibs</strong></h1>
<div>This was the first release cycle that we made use of Atlas and Atlas' Cib Editor. This was very nice for most of the views but gave us some problems on some of the more complex UIs and options. As Atlas grows and supports more features, we'll gradually move more and more of our UI code into Cibs, thereby abstracting the team from view code entirely. That'll be nice.</div>
<h1><strong>BeanCounter and Barista</strong></h1>
<div>We also released two more open source projects that have spawned because of our project. <a href="http://github.com/chandlerkent/BeanCounter" target="_blank">Chandler's BeanCounter</a> is an extendable metrics tool that the team uses in order to generate metrics for our project (we'll analyze the data in a later post). <a href="http://github.com/hammerdr/barista" target="_blank">Barista</a> is my project that enables BDD style testing of UIs through a script.</div>
<h1><strong>Server administrators</strong></h1>
<div>I have a newfound respect for them after having to troubleshoot some of the problems that we were getting with PHP and Apache. Respect, dudes. I never want your job.</div>
<h1><strong>Lessons Learned</strong></h1>
<div>I'm not sure about the rest of the team, but I feel that this was far too big of a release cycle. While I am very happy with the result (I think this is the best release cycle yet), it was hard for us to meet our own deadline because of our inability to estimate the longer release cycle. Also, our project metaphor may not be the way that the client and/or users want the application to work. If this is true, we may need to backtrack and lose 2-3 weeks of work.</div>
<div>Also, refactoring works. There was no better example of this in my experience than with this release cycle. Because we spent a lot of time making the code better, implementing the glossary was almost trivial.</div>
<br class="final-break" style="clear: both" />
