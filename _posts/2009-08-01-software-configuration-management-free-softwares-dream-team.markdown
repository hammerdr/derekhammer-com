--- 
wordpress_id: 62
layout: post
title: "Software Configuration Management :: Free Softwares Dream Team"
wordpress_url: http://blog.derekhammer.com/?p=62
---
I have taken interest in software configuration management over the past few months and after going in knee-deep, I found that <em>it isnít easy</em>. I canít go to Stack Overflow and ask ìWhat is the best SCM stack for my situation?î and get a concrete answer. Instead, the ëbestí answer would likely point you to several source control systems (e.g. git, svn, mercurial), several continuous integration solutions (e.g. nant, hudson, cc.net, cc.rb) and several issue tracking systems (e.g. lighthouse, unfuddle, trac, team system). Add into that the mixture of cloud services such as GitHub and Lighthouse along side self-maintained solutions such as an SVN server and Team System, and you have a pretty confusing set of tools and practices that secure, track, maintain, monitor and store your source code. In this murky, unsexy area of software development and software engineering, what <em>are </em>the best tools?

<!--more-->

First, let me note that there is no ìbest set of toolsî when it comes to SCM. There are tools that perform better in some situations and there are tools that different developers prefer over another. What Iím going to describe below is a <strong>versatile</strong> set of Free Software that could potentially solve your configuration management problems. However, I would suggest having at least one SCM expert on your team / in your organization so that you can get the <strong>best solution in your context</strong>.

<strong>Source Control</strong>

Quickly, I am going to describe the source control spectrum. There is obviously no source control. This has been called categorically Bad. I tend to agree. There is the manual versioning, manual backup method where you copy the source files to some shared or unshared storage for purposes of ìbackup.î Then, there are modern source control systems. Modern source control systems, despite being modern, have varying approaches to source control. There are systems that control all source from a central location (e.g. Team System). Each file must be ìchecked outî from the server before it can be edited. There are systems that have a central server but allow for local editing and ìmergingî of changes into the central server (e.g. ClearCase, SVN). Finally, there are systems that have a ìlocal repositoryî that sits between the central server and the ability to edit files locally (e.g. Git and Mercurial).

This can be a ìreligiousî subject. Some people love SVN. Some people love Git. Some people donít care. I am a fan of Git / Mercurial due to the fact that each has its own ìlocalî repository. These local repositories allow for personal source control without polluting the central server (read: Continuous Integration server) with your not-quite-done code. However, the nice thing about the tools that I am suggesting is that they are, for the most part, source control agnostic.

In most cases, I would choose Git as my repository of choice. Mercurial is very similar to Git except that its ìcloudî features are not as well developed. Also, when choosing Git, I highly suggest using GitHub. GitHub is a website that will manage your Git repository, <em>for free.</em> It will also allow for the browsing of your source code online without any additional hassle. It is a great way to manage and share your code without having the hassle of setting it up yourself.

In all cases, I would suggest using a modern source control system of some sort. I would explain the reasons, but those reasons have been highly commented on by people much smarter than I. However, it may not be prudent to try to force Git into a project. In particular, I can think of an instance where introducing Git would cause too much friction with a team because theyíve use X Source Control and like it. In this case, noticing that the modern source control system most likely offers almost all of the benefits of Git without causing that team friction would be wise.

<strong>Continuous Integration</strong>

CI is a concept that has been around for a while but has been slowly sneaking its way into software engineering practice. The term, coined by Martin Fowler, means the automation and notification of build and auxiliary processes at critical points in time. In simple terms, this means that every check-in is built, automatically, and the results are sent to the team. However, it could also mean that one build sets off another upon completion, or that a log file is written, or that a notification is sent to Twitter, or that static code analysis for checking coding standards is run.

Continuous integration is important for several reasons. First, it automates a process. Automation reduces time and error. The more times that a computer sets off the build than a person does, all the more time is saved. In addition, human error is a significant component to error in repeated processes such as running tests or building after a check in. Automating the process will reduce that risk to zero. Also, continuous integration is important for its notification features. Itís important for developers to get <em>instant feedback </em>about how / why they broke the build. This reduces leadup time and leads to a faster development process. Also, it lets other members of the team know why their build isnít working! We cannot discount the importance of auxiliary processes. These are processes that help improve code, transparency, visibility, project management, etc. While these are very ìunsexyî to programmers, it helps with the grunt-work so that actual work can be done.

CI can be very basic or very involved. It can be merely building after each check-in or it could be building, testing, static code analysis, push to another server, packaging, issue tracking management, etc. To this end, I suggest using Hudson. Hudson is a great CI tool that has the ease of use that makes beginners enjoy using but the flexibility and extensibility such that powerusers can mold it to do what they want. Hudson includes a web interface and a plugin for anything you would ever need. It allows you to run build scripts in any language that you could desire and provides some pretty sweet reporting.

Again, I must stress that Hudson isnít a cure-all. Instead, it is a good starting point. For some, CC.NET / CC.rb would be more appropriate. However, I would argue that any team of some size (say, greater than 3) <strong>needs</strong> a CI solution. CI allows for teams to work more cohesively and at a quicker pace than teams without CI solutions.

<strong>Issue Tracking / Project Management</strong>

Issue tracking has been a concept that has been around for a while now. Many teams and many projects make good use of issue tracking software in order to catalog, visualize, organize and prioritize various issues and tasks. Primarily, issue tracking has been used as standalone software which only connection to those issues and tasks are through due diligence of the team.

However, with more modern issue tracking software, it is possible to easily integrate with CI servers and source control. Through extensions like githooks, it allows for automatic linking and management of source-to-issue. In addition to better automation / integration, modern issue tracking software now exists in the cloud. Whereas older software platforms, such as Trac, would require the team to manage their own server, the cloud will handle all of that plus give you some additional features.

For a stack based on Git and Hudson, I would highly recommend the Lighthouse issue tracking web application. While there are several good services such as lighthouse, lighthouse provides a couple of key features that I believe set it above the rest. However, if you donít wish to use Lighthouse, Unfuddle is another great website.

<strong>The Free Software Configuration Management Stack</strong>

So, how would a team go about setting up the Dream Team stack? The beauty is that its very simple. Hardware-wise, you need one machine that sits somewhere accessible to the team. This could be a desktop computer that is hooked into the internet or this could be a VM that sits on a server somewhere. Either way, the barrier to entry is relatively none!

First, youíll need to sign up accounts with GitHub and Lighthouse. Both are free. Both are spam safe. Youíll create a project on GitHub and follow the instructions to set up git with your machine. (<strong>Note: For you windows users out there, I highly suggest TortoiseGit</strong>) At that point, youíll have a available Git repository. Youíll then want to set up your Lighthouse account and create a new project there. Follow the steps and enter the appropriate information.

Then, youíll need to download and install Hudson on the VM. Hudson does not, by default, support Git. However, if you download the Hudson Git extension, it will handle git repositories just peachy. Hook Hudson up to the public git repository. At this point, I suggest downloading and installing some ìNotificationî plugins so that emails can be sent out and perhaps even a Twitter account can be updatd. Also, I would have each team member install the Hudson Notifier. This is a small project that connects continuously to Hudson and queries the status. That way if your teammate breaks the build, youíll know ASAP.

And, thatís it! Youíre set. No fees. No trial period. Youíve got Free Software at its best! Happy coding!
