--- 
wordpress_id: 5
layout: post
title: New blog
excerpt: I've decided to try out the Wordpress blog software. So far, setup and usage has gone really smoothly and I think I may migrate all of my posting to here.
wordpress_url: http://blog.derekhammer.com/?p=5
---
I've decided to try out the Wordpress blog software. So far, setup and usage has gone <em>really</em>†smoothly and I think I may migrate all of my posting to here. The categories on Wordpress are really nice, as well, so I think I'm going to use that instead of multiple blogs. I've also tied it into Twitter (which is tied into Facebook) so all of my posts are now more visible to my friends. Sweet.

I'm doing a lot of work to the site so there could be some downtime / weirdness over the next few days.
