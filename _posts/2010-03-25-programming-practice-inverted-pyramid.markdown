---
layout: post
title: "Programming Practice :: Inverted Pyramid"
published: true
categories:
---

In [the last post](http://www.derekhammer.com/2010/03/24/-programming-inverted-pyramid-.html) I described the philosophy of the inverted pyramid. At the end of the blog post, I promised that I would put this into practical terms. In this post I will give a video demonstration, tell you how all of your previous quality assurance techniques fit into the inverted pyramid, and describe the current state of the inverted pyramid.

## State of Inverted Pyramid

Currently, there are several tools and techniques that exist in the world today that can help us accomplish the inverted pyramid methodology. We are not reinventing the wheel. The inverted pyramid leans heavily on the specific knowledge and best practices that exist in the quality assurance industry mixed with the practices evangelized by the test driven development community.

<div class="image_with_caption on_the_left">
  <img src="http://www.derekhammer.com/images/inverted-pyramid-colored.png" alt="Current State of Tools for the Inverted Pyramid" /><br />
In this image we are presented with some "bands" of color overlaying the inverted pyramid. The red represents automated user interface testing tools such as Selenium and Celerity. The yellow represents behavior driven development frameworks such as Cucumber. The cyan represents specification test suites such as RSpec and NSpecify. Finally, the green represents xUnit frameworks such as Test::Unit and JUnit. You can see that there are no clearly defined lines that that some are more broad banded than others. For example, xUnit (green) and BDD (yellow) both touch up to three different layers of tests. Put together, they form a fairly solid testing package that can cover most of the pyramid. However, sometimes we do want redundancy in our testing (especially on very large projects!) and we will need additional testing tools and techniques.
</div>

## Two Band/Pronged Approach

For most development, and I stress that this is not the case for every project, we can ensure quality of the application by attacking the code under the umbrella of two methods: behavior driven development and test driven development. Using a behavior test suite as the higher order tests and the unit test suite as the lower order tests, we can cover the quality assurance needs of small to medium-large applications. On the extremes, different methods will need to be practiced (for example, very small programs may be sufficiently covered and understood just by unit tests).

Behavior driven development is nothing new. Dan North has been putting this into practice for over 5 years! Cucumber was created to support behavior driven development and, at least in the Ruby community, it has taken off! This is great and inspiring that the developer community is embracing BDD on some level. However, we seem to be getting it wrong. Many people look at Cucumber as a replacement for unit testing. However, if we investigate the inverted pyramid you can see that just doing a BDD style framework only covers the top half. The lower half of the pyramid--the part from which developers get the most benefit--is completely absent. People just doing cucumber will find that doing pure BDD is much more difficult that doing pure TDD. You'll also find that you start making high level design decisions before they are necessary and that your regression suite doesn't cover as many bugs.

If we view BDD as supplementary instead of contradictory, though, we can create a solid process of quality assurance. This is the two band approach. There is not much redundancy (though there is a little bit) in the tests and we cover nearly the entire stack (Cucumber doesn't run against a 'real' browser environment). Depending on environments, different flavors of the two band approach may be needed (even adding more "bands" to the mix).

## A Demonstration

So, maybe you're buying into the inverted pyramid and you want to get started. Where do you start? I've created a video below that demonstrates a basic use of the inverted pyramid against a Rails application. (Tools used: Cucumber, Cucumber-Rails, Test::Unit, Rails 3)

<object width="400" height="300"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=10428474&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" /><embed src="http://vimeo.com/moogaloop.swf?clip_id=10428474&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="400" height="300"></embed></object><p><a href="http://vimeo.com/10428474">Inverted Pyramid Demonstration</a> from <a href="http://vimeo.com/hammerdr">Derek Hammer</a> on <a href="http://vimeo.com">Vimeo</a>.</p>