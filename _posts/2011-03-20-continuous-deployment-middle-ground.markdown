---
layout: post
title: Continuous Deployment Middle Ground
published: true
categories:
---

The Hacker Chick (Abby Fichtner) [wrote](http://www.thehackerchickblog.com/2011/02/continuous-deployment-for-continuous-learning.html)
 about continuous deployment (edit: almost 2 months ago.. my bad!) and posed the question about whether it makes sense. She presents 2 possible options: 2 week iterations and continuous deployment.
 
There is a middle ground[1] between the 2 week iterations and continuous deployment. The idea behind continuous deployment has two proposed benefits. 

First, there is the technical benefit that consistently running through your whole deployment stack makes it less cumbersome and more reliable. Generally, continuous deployment encourages automation and 'hands off' deployments where very little human interaction is involved (usually just a push of a button on the server).

There is also the product management benefit. As Eric says in the lean startup, reducing the lead time from 'Go' to 'Customer Feedback' is a huge boon to product development. It helps startups (and large businesses trying to innovate) find the right product and/or market. This materializes as multiple deployments per day (though 50 seems pretty high to me).

The middle ground is to just focus on the technical benefit. IT should be able to do this without much involvement[2] with product management teams. Product management can then decide when to deploy new features (daily, weekly, monthly, erratically, etc.).

After that, you can reap the technical benefits. You can move to an iteration-less process (though you can do that without continuous deployment!). You can automate, automate, automate. You can stabilize your build and deployment pipeline. You could quickly deploy beta versions or A/B tests without disrupting your current production server. All in all, you can make better software.

[1] I love how 2 week iterations are the conservative point in this conversation! Amazing how certain ideas (continuous deployment) can really change the conversation.

[2] It can do something like blue-green deployment where they have a shadow production server that can be swapped into production at any point. This allows you to continuously deploy to a server but not expose that server to customers.