---
layout: post
title: "Cappuccino :: OJAutotest"
published: true
categories:
---

Here is an introduction video to OJAutotest. I've been working on this the past couple of days in order to make my life easier (I'm about to go into super-testing mode for our application).

<object width="400" height="300"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=9694463&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" /><embed src="http://vimeo.com/moogaloop.swf?clip_id=9694463&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="400" height="300"></embed></object><p><a href="http://vimeo.com/9694463">OJAutotest Demo</a> from <a href="http://vimeo.com/hammerdr">Derek Hammer</a> on <a href="http://vimeo.com">Vimeo</a>.</p>

The important part to know here is that this is very much a convention-based tool. Your tests _must_ be in <code>Test</code> and you must run <code>ojautotest</code> from the top level. If you are interested in using this tool, it is available <a href="http://github.com/hammerdr/OJTest">on GitHub</a>.