---
layout: post
title: "Modules in ExtJs"
published: false
categories:
---

A co-worker asked me today if javascript had modules. Modules are a ruby concept of runtime mix-ins to objects. They are great for code reuse that doesn't necessarily fit an inheritance tree but does fit a particular concern (say Logging). In Ruby:

<pre>
	module Logging
		def log message
			puts message # crude form of logging
		end
	end
	
	class ClassA
		include Logging
	end
	
	class ClassB
		include Logging
	end
	
	ClassA.new.log "first"
	ClassB.new.log "second"
</pre>

I have mentioned this before as 'javascript mix-ins' but it is especially easy if you are using ExtJs. The code below achieves the same as the above.

<pre>
	var Logging = {
		log: function (message) {
			console.log(message);
		}
	};
	
	var ClassA = function () {
		Ext.apply(this, Logging);
	};
	
	var ClassB = function () {
		Ext.apply(this, Logging);
	};
	
	(new ClassA()).log("first");
	(new ClassB()).log("second");
</pre>

If you aren't using ExtJs, then you can substitute your own apply.

<pre>
	var apply = function(target, module) {
		for(var x in module) {
			target[x] = module[x];
		}
	};
</pre>

And now we can reuse some code!