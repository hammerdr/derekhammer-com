---
layout: post
title: Unit Test Scorecard
published: false
categories:
---

We have studied what makes a piece of code "good" or "clean." For the most part, programmers can look at a piece of code and generally agree whether that particular piece is Good, Mediocre, Bad or Really Bad. Unit testing, however, seems like a black art to most people.

For the purposes of this score card, there are three levels for the rubric. Good, Mediocre and Bad. 

# Number of Assertions

The number of assertions is a quick and easy method of gauging a test. Most tests should have just one assertion. However, that isn't really the rule. The rule is that tests should only test one piece of behavior (also known as execution path). Examples:

## Good:

<code>
  var AnObject = function () {
    this.items = [];
    this.addObject = function (item) { this.items.push(item); };
    this.all = function () { return this.items; };
    this.allExcept = function (exceptions) {
      var values = [];
      for(var i = 0, n = this.items.length; i < n; i++) {
        if (exceptions.indexOf(this.items[i]) === -1) {
          values.push(this.items[i]);
        }
      }
      return values;
    };
  }

  var testAddObject = function () {
    var target = new AnObject();
    
    target.addObject(1);
    
    assertEquals(1, target.all().length);
  };
  
  var testExceptions = function () {
    var target = new AnObject();
    
    target.addObject(1);
    target.addObject(2);
    target.addObject(3);
    target.addObject(4);
    
    assertEquals([1,3], target.allExcept([2, 4]));
  }
 
  var testIdempotency = function () {
    var target = new AnObject();

    target.addObject(1);
    target.addObject(2);
    target.addObject(3);
    target.addObject(4);

    assertEquals([1,3], target.allExcept([2, 4]));
    assertEquals([2,3], target.allExcept([1, 4]));
  }
</code>

These are good because they all test one thing (even though the last one has 2 assertions). The first is testing that adding an object will actually be added. The second is testing the logic behind allExcept. The last is testing the idempotency (lack of side effects) of allExcept.

## Mediocre

<code>
  // in this test we are testing that allExcept works and is idempotent
  var testAllExcept = function () {
    var target = new AnObject();

    target.addObject(1);
    target.addObject(2);
    target.addObject(3);
    target.addObject(4);

    assertEquals([1,3], target.allExcept([2, 4]));
    assertEquals([2,3], target.allExcept([1, 4]));
  }
</code>

In mediocre, we are taking a bit of a shortcut. We are consolidating the last two tests of the example above. This would seem to make sense except that when this test breaks we will have to figure out why it fails. The testing isn't really informing us why it breaks because it is testing two different things. It could either have side effects now or just be a broken implementation. This is not the end of the world but can be very frustrating when you start adding 3, 4, 5 or more variables into the mix.

## Bad

<code>
  var test = function () {
    var target = new AnObject();
    
    target.addObject(1);
    
    assertEquals(1, target.all().length);
    
    target.addObject(2);
    
    assertEquals(2, target.all().length);
    assertEquals(2, target.allExcept().length);
    assertEquals(1, target.allExcept([2]).length);
    
    target.addObject(3);
    
    assertEquals(2, target.allExcept([2]).length);
  }
</code>

In bad, we are shoving all of our tests and a few more into one test function. While this technically would cover all of the bases, it is very difficult to understand. Idempotency, for example, is tested because allExcept([2]) is used twice. However, that is not very clear from the code written. This test theoretically covers 4 or 5 actual unit tests. If this test broke, we would be only a little more informed than we were and would either need to break the unit test up or run a debug.

# Organization

The organization of a unit test matters for readability purposes. A good unit test will follow a basic pattern. In xUnit, this happens, in general like this:

<code>
  TestClass
    - SetUp
    - TearDown
    - Test 1
      - Set Up (Objects, Mocks, Dependencies)
      - Actions (Calling of method under test)
      - Assertions (Hopefully just one!)
    - More tests...
</code>

It is less important that you follow this general pattern but that you follow *a pattern*. Putting tear downs at the bottom isn't illogical and wouldn't cause a "Good" to become a "Mediocre" as long as you are consistent.

## Good

Consistent, logical organization. Set up objects are organized in a logical manner (e.g. a mock object expectation being defined in close proximity). Quickly scannable test methods that help readers identify key areas.

## Mediocre

Generally follow a consistent pattern. Sometimes things are out of place and lead to confusion.

## Bad

No pattern is discernible. Or, patterns are mixed and matched throughout the tests.