---
layout: post
title: "Build Javascript Objects"
published: false
categories:
---

If you are starting a new web application project, chances are that "javascript application" meets the specifications somewhere. When tackling your new javascript app, you'll run into several critical decisions about the nature of javascript and how you should handle the 'appiness' of your client. My team and I recently discussed the talkiness of the client.

# When creating a domain object, when should we communicate with the server?

You should communicate with the server when the object is done being built on the client. Typically, in a website, you build an object in a single web form. In richer applications, a wizard could be used. Or a series of drag and drop actions that piece together a full object. The 'transient states' of each wizard step or each drag and drop action do not need to be communicated to the server!

Why not? We've done it that way with web applications for a while now. Why the sudden change in philosophy? First, it allows us to radically reduce the complexity of the server. The server can become a data store with business rules validation.

# An Example

So, how do we pull it off? Let's take the example of creating a baseball team. The baseball team needs a name, location, manager (who is herself a person) and players (all people). We'll take a wizard style approach to this (because the UX dude said so!) and put name / location on the first step, manager on the second, player selection on the third and a review before finishing.

<pre>
	var Wizard = function () {
		this.steps = [nameLocation, manager, players, review];
		...
	};
</pre>

We start by giving the Wizard a blank team.

<pre>
	var Wizard = function () {
		this.steps = [nameLocation, manager, players, review];
		this.team = {};
	};
</pre>

For nameLocation, we want it to update the team on success.

<pre>
	var Wizard = function () {
		var nameLocation = new NameLocation(function (name, location) {
			this.team.name = name;
			this.team.location = location;
		});
		this.steps = [nameLocation, manager, players, review];
		this.team = {};
	};
</pre>

For players, we want it to update the team with the player list. For manager, we want to do the same.

<pre>
	var Wizard = function () {
		var nameLocation = new NameLocation(function (name, location) {
			this.team.name = name;
			this.team.location = location;
		});
		var manager = new Manager(function (manager) {
			this.team.manager = manager;
		});
		var players = new Players(function (players) {
			this.team.players = players;
		});
		this.steps = [nameLocation, manager, players, review];
		this.team = {};
	};
</pre>

And, finally, we want to commit the changes to the server on the review.

<pre>
	var Wizard = function () {
		var nameLocation = new NameLocation(function (name, location) {
			this.team.name = name;
			this.team.location = location;
		});
		var manager = new Manager(function (manager) {
			this.team.manager = manager;
		});
		var players = new Players(function (players) {
			this.team.players = players;
		});
		var review = new Review(function () {
			$.ajax({data:this.team});
		});
		this.steps = [nameLocation, manager, players, review];
		this.team = {};
	};
</pre>

In this, we are gradually building up the Team object in the client (and in the client only!). The server doesn't know about the object until save is hit on review.