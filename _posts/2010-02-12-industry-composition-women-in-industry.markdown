---
layout: post
title: "Industry Composition :: Women"
published: true
categories:
---

_I have been mulling this post over for a while now and, due to an inability to sleep, I finally have some time to put my thoughts down._

In the software development world, there is a serious shortage of women in the computing industry. Women only account for about ten percent of the computing industry. Contrastingly, in life sciences such as medicine and biology, women make up about half of the industry! We, as a profession, are losing out. We, as a profession, are missing a huge opportunity. We, as a profession, need to do something about it.

# The Opportunity

Some people may ask: so what? Many would respond with something akin to it being a social injustice or values of a male-dominated society. And, certainly, those play a part in the overall picture. However, that isn't why I am so concerned. _For the record, I believe there are some places in our industry that do participate in social injustice but that the majority of people and companies in **our industry** are not so afflicted._

Instead, the main reason for my concern is that women provide a diversified workplace. When I work with someone, I do not want to work with a Yes-Man. I want someone that has a different perspective about code, engineering, life, etc (to a point). I want to work with someone that challenges my ideas, that forces me to revisit and think through my assumptions. That is only achievable by putting people of different backgrounds together in the same room.

It is unfortunate that when people think about diversity, many of them think about skin color. How many African-Americans do you have in your employ? How many people from China, India and Japan do you work with? Instead, diversity is about experiences. I cannot think of a more divergent experience than that which exists between men and women. These differences in experience are universal across the world, across racial boundaries, across cultural boundaries. Women and men really are from different planets and _we need both._

That is not to say, however, that cultural, racial or regional diversity is to be discounted. Instead, we should embrace them, as well. The more diversity, the better.
 
# The Good News

There is some good news. 

Several universities realize this lack of women in our industry and are implementing programs to encourage women to get degrees in computing. According to the Department of Labor, the number of women expected to graduate from computing programs is expected to increase dramatically by the end of this year. However, they do not predict that there is any increase in percentages of women in those graduating classes. These universities are pursuing the right course of action by creating program in order to increase participation of women.

Unlike other industries that require higher education such as law, medicine and life sciences, the salary disparity between men and women is almost miniscule. Women make under two percent less than men, on average, in our industry. That is truly remarkable when compared to the salary disparity between men and women in the medical industry: an astonishing 21% (in industry). It seems to me that when a woman decides to become a member of the computing industry, she is respected more than she would be in other industries. Awesome!

# The Bad News

Universities are not doing nearly enough. Universities should sponsor aggressive campaigns to recruit women. This means creating local computing competitions, hiring more women in computing as speakers for events, advertising the positives about the computing industry (like the comparison to the medical industry above), etc.

Companies are not doing nearly enough. Many companies do not and never will care about the advancement of our industry as a whole and that is entirely their loss. However, responsible companies need to realize that gender diversity is a serious issue and that they, too, need to actively support it.

# Conclusion

I know that my professors, especially Sriram, are going to disagree with me on the fact the universities are being passive about this. He actively participates in a group that is devoted to increasing gender diversity in our profession. The invited a professor from another university who is a specialist in this area to visit and give suggestions (She was wicked awesome, by the way. I wish I remembered her name.). However, the graduating class of 2010 at Rose-Hulman Institute of Technology has one female in the Computer Science and Software Engineering degree programs. One!

# Sources

I pulled from the following sources to make this article. I didn't make these numbers up, I promise!

* http://www.dol.gov/wb/factsheets/hitech02.htm
* http://www.bls.gov/oco/cg/CGS033.htm 
* http://www.medicalwomensfederation.org.uk/Campaigns/Pay%20gap%20report.htm