--- 
wordpress_id: 121
layout: post
title: "Objective-J :: Testing Cappuccino UI"
wordpress_url: http://blog.derekhammer.com/?p=121
---
<p style="clear: both">For those testers of Objective-J, there are difficulties testing Cappuccino. One particularly sticky area is Views. Unit testing these suckers is impossible because the DOM doesn't exist in OJUnit. Not true! We can actually use OJMoq to subvert (oh we're sneaky!) the Views and make them<em> </em>think that they are talking to the DOM.</p>  <p style="clear: both"><em>Note/Disclaimer: I'm not saying that everyone should use this method of testing. In fact, views should be in the form of CIBs and real testing should be with the usability of the product. Put the logic in the Controllers and test those! This is just a "because we can" post and you'll note that I never use it in OSL to test views.</em></p>  <p style="clear: both">So, I'm going to drop a big view file on top of you. You can really just ignore that, but pay attention to the OLFeedbackWindowTest.j file. It's small but contains important information.<br /><br /></p> [gist id="237044"] <p style="clear: both">You'll notice the two lines</p>  <pre>CPApp = moq();
CPApp._windows = moq();</pre>  <p style="clear: both">at the beginning of that test file. They are what allow us to subvert the views. Two lines and previously untestable areas of your code are now testable! Good luck testing, people!</p>  <br class="final-break" style="clear: both" />
