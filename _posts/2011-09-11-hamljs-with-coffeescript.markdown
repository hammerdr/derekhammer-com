---
layout: post
title: HamlJS with Coffeescript
published: true
categories:
---

I am currently playing around with a NodeJS stack where I am writing Coffeescript for my application. To render my views, I am using Haml. HamlJS only used javascript. So, here is a quick and dirty plugin that lets you use coffeescript in those views! I apologize for the lack of tests and please let me know if there are any errors that you find.

[NPM Registry](http://www.google.com)

[GitHub](http://www.google.com)