---
layout: post
title: Introducing CuCapp
published: true
categories:
---

CuCapp is Cucumber for Cappuccino. Yep. And, this time, its real cucumber. It's not the weaksauce I started with Barista (I'm discontinuing Barista development--it was a great learning experience and very tough; CuCapp should be a better and more fully featured replacement now). I should mention that Daniel Parnell started the project and that I just forked it on github and put some elbow grease into it.

<object width="400" height="300"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=11543135&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" /><embed src="http://vimeo.com/moogaloop.swf?clip_id=11543135&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="400" height="300"></embed></object><p><a href="http://vimeo.com/11543135">CuCapp</a> from <a href="http://vimeo.com/hammerdr">Derek Hammer</a> on <a href="http://vimeo.com">Vimeo</a>.</p>

Pretty, right?

OJUnit, OJMoq, OJAutotest, OJCov, OJSpec and CuCapp. I'm pretty sure we're getting somewhere with testing.

I'm going to be constantly improving all of these tools. Bringing OJSpec up to speed is next on my list, but perhaps you guys have better suggestions? What do Capp devs want to see for testing?