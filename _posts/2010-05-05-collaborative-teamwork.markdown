---
layout: post
title: Collaborative Teamwork
published: true
categories:
---

_The following is an excerpt of an essay that I had to write. I thought it expressed how I felt about my current team and that it may be useful to people forming their own teams._

# The Collaborative Team

On our project, Project OSL, we are working on a project that outstrips the level of experience each of us has attained individually. At the beginning of the project, we were faced with a daunting project but through sharing ideas, supporting, teaching, learning and communicating with each other, we were able to create a successful experience for the whole team.

## Sharing Ideas

Our project is both ambitious and unspecific. We were to create a piece of software that relied on unproven technologies that would match the ambitious vision of our client and ourselves. The technical details and user needs were mostly unknown beyond the high level curiosities.

In order to overcome that obstacle, the team needed to constantly bounce ideas off of each other. We would usually work together in a shared space so that we could look up and ask a team member to head over to the whiteboard and listen to ideas.

We came up with some bad ideas; we came up with some good ideas. Each time, the feedback from our team members was essential in solving the tough problems.

## Supporting

Despite the fact that we constantly asked each other for input on ideas, we were never negative toward each other. We all understood that the goal was a solid product for our client and that the criticisms were constructive rather that destructive and personal.

## Teaching

We are constantly teaching each other. Whether it be a formal seminar to our teams, an informal solve-a-hard-coding-problem-as-a-team dojo, or just a "How does this work?", each member of the team became a teacher at one point or another.

## Learning

With the willingness to teach, there is a necessary desire to learn. The team was always ready to learn something new from each other and this collective sharing of knowledge allowed us to be flexible and strong as a team.

## Communicating

Each of the above were compounded by the fact that our team is effective at communicating ideas. We were able to stand up at white boards and create quick modeling diagrams in order to describe what we were thinking; we were able to create presentations that were quick, to the point and wasted no ones time; we were able to draft effective emails; and, most importantly, we liked interacting with each other. 

Both of my teammates are guys that I can hang out with. They are certainly not my best friends but they are certainly good friends. I have had beer with both. I've had (completely unnecessary) dinner with both. None of us grate on each other.

Communicating effectively makes all of the above attributes of the team work cohesively and effectively.
