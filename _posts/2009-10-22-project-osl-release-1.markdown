--- 
wordpress_id: 82
layout: post
title: "Project OSL :: Release 1"
wordpress_url: http://blog.derekhammer.com/?p=82
---
We have just finished our <a href="http://github.com/hammerdr/Omni-Software-Localization/tree/Release-1" target="_blank">first release of Omni Software Localization</a>. The feature set is very basic and what the user interacts with seems very.. rudimentary. This is expected. Here are the features that are currently supported:
<ul>
	<li>Upload Localizable Resources</li>
	<li>Edit Localizable Resources (Current Supports PList <strong>only</strong>)</li>
	<li>Select and Download Localizable Resources (Currently Implemented, but not accessible via UI)</li>
</ul>
This is a small, but high risk subset of our features. We also ran into a bug during development. We were unable to get Apache set up correctly on the Production server. Check <a href="http://twitter.com/projectosl" target="_blank">@projectosl</a> for updates on when we have fixed this.

In the next release, we are going to implement a user feedback system so that we can get feedback from everyone out there! That release is planned for <strong>November 3rd</strong>. So get ready to start giving us some feedback!
<h1>Lessons Learned</h1>
After the first release cycle, we learned a few things. Bugs tend to manifest themselves at the end of a cycle. Automation really does help with pushing to a test server and/or production server. Testing prevents bugs in refactoring. That our time we spent refactoring and cleaning code was not wasteful.
<h2>Bugs</h2>
We didnít find bugs during development during the first 1.5 weeks. I think it is because of the mentality that ìweíll get to it later.î So, the bugs manifest at the end of a cycle. This is fine as long as we analyze and prioritize these bugs in the next release cycle (as we have done).
<h2>Automation</h2>
Automation is a life saver. It removes all of the pain and agony that happens throughout development. Our team has scripts for pushing to test server, pushing to production server, automated continuous integration, git scripts, and documentation scripts. These automation scripts mean that we <strong>no longer have to think about any of those operations</strong>. It reduces the mental load of development and allows us to focus on <strong>producing value</strong>.
<h2>Testing</h2>
I think this should be a given, but testing has been a positive thing for our team. We do not feel as if we have ìwastedî time on tests and have ensured that refactoring / adding features do not break existing code. Also, testing helps us <strong>manage dependencies</strong>.
<h2>Refactoring</h2>
Weíve seen the effects of refactored code already. The code is easier to read and manage. It also sets up a better design that allows us to add, edit or remove features from the system. For example, in our system, we wanted to have a more Mac-like interface. We changed no code in either the Models or the Controllers and only changed the View class for that particular object. It was a good feeling that we are handling separation of concerns well.
<h1>Software Architecture</h1>
Our software architecture is starting to take shape. It follows the <a href="http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller" target="_blank">MVC pattern</a> but is slightly modified. A more detailed version of the architecture will be outlined by Chandler in the near future.
<h1>Metrics</h1>
The following metrics were obtained from our metrics generating script.
<pre>Number of Methods: 63
Number of Tests† : 26
Tests per Method : .412
Number of Classes: 24
Methods per Class: 2.625</pre>
Our tests per method is way too low. Weíll hopefully rectify this in the next release. This is most likely due to the fact that a lot of our code is UI code which, as of yet, is untestable.

Our methods per class, however, is very good. It is hovering around 3 which is a good place to be. This means that classes are likely being responsible for only one thing.
