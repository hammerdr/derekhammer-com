---
layout: post
title: "On Business"
published: true	
categories:
---

We find ourselves with companies that make it big and then come tumbling down. This is part of the capitalist cycle. Tragedies such as Enron and the Lehman Brothers do not have to be. The solution starts inside corporate culture. Business, by and large, has been locked in by the mindsets of business schools. Technology companies are no different.

ThoughtWorks has led the way in the past 10 years to get technology groups to adopt agile practices inside of their development work. While there are still holdouts, there are companies around the world pair programming and Kanbaning their way into successful products. There is still work to be done on that front but I believe we can hold a party on an aircraft tanker at this point.

What happens now?

There has been talk over beer about the future. What does a post-agile world look like? Agile has problems but the framework is really en enabler for development teams to experiment and adopt new practices continuously. In theory, we should be able to be hands off. So where do we innovate? The future cannot be to just bring the last bastions of waterfall development under our wing. That isn't sufficiently ambitious for myself or the people with whom I have shared this conversation.

We want to change how businesses work. In 10 years, we want Harvard Business School to teach the New Way of business, even if it's grudgingly. Right now, we don't have all of the answers. Right now, we have identified some of the problems. Right now, we have very few solutions. 

We think that we can apply the values (but not necessarily the policies or practices) of Agile to the business world. Values such as communication and trust are almost certainly a component to the solution. We are still working this through. We are still experimenting. We are still evolving our understanding of this problem and solution.

Right now, this is a goal of a small group of people inside of ThoughtWorks. I am one of them and I have been part of several conversations. It's something that some of us feel very strongly about. We want to create a new identity and legacy for ThoughtWorks in the coming decade.

Crazy? Yeah. Crazy enough that it just might work.