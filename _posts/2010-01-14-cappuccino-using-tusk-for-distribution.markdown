--- 
wordpress_id: 157
layout: post
title: "Cappuccino :: Using tusk for distribution"
wordpress_url: http://blog.derekhammer.com/?p=157
---
<p style="clear: both;">You're working with Cappuccino or Atlas, and you say something like "I really wish that Cappuccino had something like X." Then, you take the initiative and create X. It's a great idea and you execute it perfectly. It works great on your machine and now it is time to share X with the world.</p>
<p style="clear: both;"><strong>How do I do that?</strong><strong>
</strong></p>
<p style="clear: both;">Before package management systems existed, the only way to distribute this was to publish your source or a binary, have people stumble on to your site and download them directly. They would follow the instructions and hopefully everything would work out. Then they would have to understand how to import the files correctly and embed them inside of their application themselves.</p>
<p style="clear: both;">Along came package management systems such as rubygems and all of this became abstracted away from the user. Instead of installing into the project itself, gems could be installed in a central place on the machine. Users could simply type</p>

<code>gem install X</code>
<p style="clear: both;">and X would be installed, usable and all while never having to know how gem works.</p>
<p style="clear: both;"><strong>So I can use gems?</strong></p>
<p style="clear: both;">No, not exactly. Gems, as far as I know, works for Ruby and only Ruby. Don't get too disappointed, though. There is a very good package management system for CommonJS (CommonJS is compatible with Objective-J). It is called tusk. Currently it is an extension of the Narwhal system (if you have cappuccino, you have narwhal; if you have narwhal, you have tusk). And, just like gems you can type</p>
tusk install X
<p style="clear: both;">Saweet. Except, you have to do some more work to get that working.</p>
<p style="clear: both;"><strong>What do I have to do?</strong><strong>
</strong></p>
<p style="clear: both;">There are a couple of things that need to happen. First, you need to make X into a CommonJS package. <a href="http://wiki.commonjs.org/wiki/Packages/1.0" target="_blank">The package standard was just finalized a few days ago</a>. You can go read the standard and modify your package to meet the standard, or..</p>

<pre>tusk init</pre>
<p style="clear: both;">Yep. That command creates a baseline package for you to work from. Lets look at what that creates:</p>

<pre>X/
    /-README
    /-bin/
        -/activate
        -/activate.bash
        -/sea
    /-lib/
    /-narwhal.conf
    /-package.json</pre>
<p style="clear: both;">Turns out that bin and lib are not needed (unless you want an external command, not a capp library. The bin should contain a shell script to do that if you want). You can remove those. You do need to look at the contents of the package.json, though.</p>

<pre>{
    "name": "",
    "author": "",
    "dependencies": [],
    "contributors": []
}</pre>
<p style="clear: both;">Name is the name of your project ("X"). Author is you. Dependencies for most Capp libraries should be ["objective-j", "cappuccino"]. And contributors are others that have hacked on your code. I also suggest adding "description" and "keywords" to the package description. Now, here is the important part. You <em>must</em> include "objj-frameworks" which should have the following value ["Framework"]. This is telling narwhal where your code is (we'll get to putting your code there in a second). This is OJMoq's package.json for comparison:</p>

<pre>{
    "name": "ojmoq",
    "dependencies": ["narwhal", "objective-j"],
    "author": "Derek Hammer",
    "description": "A mocking library for Objective-J that is inspired by the Moq project.",
    "keywords": ["objective-j", "unit test", "testing", "test", "mocking"],
    "objj-frameworks": ["Framework"]
}</pre>
<p style="clear: both;"><strong>Where's my code?</strong></p>
<p style="clear: both;">So, we've set everything up for packaging. Except for your code. This is actually quite simple, but there are a few things you need to know. First, let's visit Cappuccino Frameworks. When you import CPObject, this is how you do it:</p>

<code>@import &lt;Foundation/CPObject&gt;</code>
<p style="clear: both;">This is because Cappuccino puts the following in the "Framework" folder of its narwhal package</p>

<code>Framework/Foundation/CPObject.j</code>
<p style="clear: both;">Do you see the simple relationship? The first part of your library is the "Framework Name" and the second part is the actual file. For most Capp libraries, it'll probably be something like this</p>

<code>@import &lt;OJMoq/OJMoq.j&gt;</code>
<p style="clear: both;">So, for your library called X, you should put code in "Framework/X/." Now you're done. You will soon be about to import like this:</p>

<code>@import &lt;X/X.j&gt;</code>
<p style="clear: both;"><strong>Almost? There's more?</strong></p>
<p style="clear: both;">We're almost done. Two more steps need to happen. First, you need to put your code somewhere online in a zip archive. Tusk will download from that location. If you have your code on GitHub, though, you're already set. GitHub can create zip files of you code without any additional setup.</p>
<p style="clear: both;">Finally, you need to have your package pulled into narwhal. You do this by editing the sources.json and asking the narwhal guys to pull your change. There are many different ways to fill this part out depending on how you make your zipfile available, but there are plenty of examples in the sources.json and you should be able to figure it out.</p>
<p style="clear: both;">Good luck and hopefully we'll see some Capp packages to play with!</p>
<br class="final-break" style="clear: both;" />
