---
layout: post
title: "OJMoq :: v0.3 Released"
published: yes
categories:
---

Version 0.3 of OJMoq was just released. I'm going to be very brief. I'll dig into some of the more technical details in a later blog post because this is part of my refactoring independent study.

## API

The API has changed. The following methods have been deprecated:

<pre>
- (void)expectSelector:(SEL)aSelector times:(CPNumber)times
- (void)expectSelector:(SEL)aSelector times:(CPNumber)times arguments:(CPArray)arguments    
- (void)selector:(SEL)aSelector withArguments:(CPArray)arguments returns:(id)value
</pre>

and they have been replaced with

<pre>
- (void)selector:(SEL)aSelector times:(CPNumber)times
- (void)selector:(SEL)aSelector times:(CPNumber)times arguments:(CPArray)arguments    
- (void)selector:(SEL)aSelector returns:(id)value arguments:(CPArray)arguments 
</pre>

While the old selectors still work, I'm not sure if v0.4 will have them. So, you should be migrating these to the new version. I changed these in order to be more consistent and predictable.

## Base Objects

In v0.2, I basically rendered the base object (the object that OJMoq wraps) inert. It didn't really do anything. In v0.3, I gave developers the option to make the base object valuable. Before, <code>moq()</code> and <code>moq(@"SomeString")</code> did the same thing. That is not true now. If you pass <code>moq()</code> a non-existent selector, it will eat it (like before). If you pass <code>moq(@"SomeString")</code> a non-existent selector, it will throw an exception. Be sure to only pass objects that aren't dangerous as base objects!

## Callbacks

Version 0.3 supports callbacks. Using an api that you would expect (<code>selector:callback:</code> and <code>selector:callback:arguments:</code>) it expects to be passed a single-argument function. The function will be passed an array of arguments that was passed directly as the arguments of the selector you are expecting. For example,

<pre>
- (void)someAction:(id)someObject
{
    [someObject a:someA b:someB];
}
</pre>

and the test

<pre>
- (void)testThatXDoesSomeAction
{
    var target = [X newX];
    var myMoq = moq();
    
    [myMoq selector:@selector(a:b:) callback:function(args)
    { 
        console.log(args[0]); // a 
        console.log(args[1]); // b
    }]
    
    [target someAction:myMoq];
}
</pre>

(Note: this is a bad test. It doesn't assert anything. Be sure that you're checking something!). Now we can do some cool behavioral expectations. Good. Now lets go test!