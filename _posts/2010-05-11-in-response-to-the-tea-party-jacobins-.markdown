---
layout: post
title: "In Response to 'The Tea Party Jacobins'"
published: false
categories:
---

I just finished reading [The Tea Party Jacobins](http://www.nybooks.com/articles/archives/2010/may/27/tea-party-jacobins/?pagination=false). It is a well written piece that attempts to put its finger on the source and direction of the Tea Party movement. He does a great job on finding the source of the movement but the direction he sees the party moving is off by a bit (which is the source of nearly all his criticisms).

As someone that was at the heart of the beginning of the Tea Party movement, a Ron Paul Republican, a little-L libertarian and true believer of this political philosophy, I have a unique perspective. I was conversing with the leaders of the original rallies, was one of the first people to donate to the original Tea Party (which wasn't even a political rally but instead was a 'Money Bomb' for Ron Paul!), was going point to counter-point on how we could get people to join in this movement. I am not approaching this as a historian, but as someone who did (and still does) _feel_ the Tea Party movement.

# The Source

The first half of the article was pretty much spot on. The Tea Party movement identifies with the counter-culture of the 1960s as well as the populist economic bent of Reaganism. Everyone involved was a libertarian of some sort: whether a Ron Paul Republican or a Libertarian Party card carrying member. Our heroes were the civil and economic rebels cast throughout the 20th century. We didn't look up to Rush Limbaugh or Glenn Beck; we looked for our inspiration from dyed in the wool counterculture icons or classical liberals. Barry Goldwater. Woodrow Wilson. Ron Paul. Ed and Elaine Brown. These were our heroes.

In fact, though the tone of the article was a bit derisive, I read the first half as a glowing review and spot on identification of our movement. I feel no embarrassment in any of those ideals. I embrace that I have no faith in government!

# Anti-Intellectualism?

