--- 
wordpress_id: 30
layout: post
title: Importance of Comfort
wordpress_url: http://blog.derekhammer.com/?p=30
---
<p>As I was setting up Visual Studio to my normal preferences, I was considering how important it is to actually go through that process. Every time I install Visual Studio, I go <a href="http://blog.wekeroad.com/blog/textmate-theme-for-visual-studio-take-2/">directly to the WekeRoad blog and grab the Vibrant Ink theme for Visual Studio</a> (then modify it to have 14 pt. font). This, I believe, makes coding more enjoyable. I think that the Vibrant Ink theme is easier on my eyes (I have slight light sensitivity) and just <em>feels good</em>. So, I was wondering how important this actually is. Does it, over time, improve my performance?</p>  <p>For that matter, does sitting in a comfortable desk chair make engineering more productive? Or ergonomic keyboards? Or even having nice backgrounds? I think it does, just because of the fact that happier people produce better code. However, it would be interesting to see a study of some sort on this topic, in general, and editor styling in particular.</p>
