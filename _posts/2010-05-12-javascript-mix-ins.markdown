---
layout: post
title: "Javascript Mix Ins"
published: true
categories:
---

When writing our compiler, Chandler and I had to go through several phases of development. Each of these phases were adding one or two methods to our node objects in our abstract syntax tree. When we need to add typechecking, we had to write a method for each node type that would know how to typecheck itself.

Unfortunately, this meant that we were constantly modifying the node classes. This breaks the open/closed principle and just feels bad. We didn't want to keep changing the class to extend it.

Additionally, by adding these to the existing classes, we were muddying the code. The purpose of the class would seem lost in the sea of additional code we would add. The code we added would also lose information density. Having related code in close proximity allows for greater understanding of what is going on inside of the code.

What we really wanted were Ruby-esque mix ins. And, despite not having any capability provide explicitly by the language (like it is in Ruby), the flexibility of javascript allowed for us to provide this functionality.

The class with the mix in "mixed":

<pre>
var MyMixIn = require("mixin");  
  
var MyClass = function() {
  for(var key in MyMixIn) {
    this[key] = MyMixIn[key];
  }
};

MyClass.prototype.doSomething = function() {
  
};
</pre>

The mix in:

<pre>
exports.doSomethingElse = function() {
  
};
</pre>

And, using it:

<pre>
var obj = new MyClass();
  
obj.doSomething();
obj.doSomethingElse();
</pre>

Good stuff. Now we can package our related code, reuse common methods through mixins and other crazy cool stuff that involves mixins.