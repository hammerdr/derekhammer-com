---
layout: post
title: The Perfect Deploy
published: false
categories:
---

Deployment is a simple word for a complicated procedure. In every organization, there are a variety of processes and tasks that need to happen to determine if deployment can and has succeeded.

## Successful Deployment

To truly understand the scope of deployment, we need to evaluate the success parameters of a deployment. I proffer that a successful deployment:

* delivers desired functionality
* has no degradation of service to users
* has no cost
* is validated
* is quick

Desired functionality refers to the fact that we will want to deploy features to the world when and only when business decides the feature is ready for real world use. This means that we need a way to deliver new Feature B but not new Feature A.

No degradation of service to users is mostly about downtime. It can also mean that users shouldn't have to reauthenticate or in any way disrupt the use of the service.

No cost has huge implications: it should be automated and not need human intervention. It should not negatively impact the ability of the service to handle user load. It should not require manual QA checking before and after the service.

Is validated is the act of validating that the deployment was successful. In almost all cases, this is done manually. However, combining this with no cost seems to indicate that manual QA may not be the solution.

And quick refers to the fact that the lag time from decision to go live and the actual go live should be as low as humanly possible. In fact, maybe the decision maker should have control of 'The Button'.

## The Perfect Deploy

By that criteria, no one in the world successfully deploys software. The criteria is far too strict to be a true definition of 'successful' but it does describe the ideal state that an organization would be in.

The point that I want to make is that deployments are not perfect in anyones world and that we need to make trade offs for the different business cases. For example, my current team hits only 3 of the above 5 points completely. Cost is a big one that seems especially difficult to overcome (and may, in fact, be impossible to attain).

So, if we aren't perfect and we need to make some trade offs, how do we make those kinds of decisions?

## Know your domain

The first job to figuring out how to improve your deployment process is to know your domain. For example, if your application is a high volume trading application that gets deployed during the stock exchange down time (of which is down about 12 hours), then perhaps degradation of service is not what you should focus on. However, if you have a high volume, real time system that goes 24/7, perhaps zero downtime deployment is your main constraint.

The real world fact is that we do not have the luxury to make everything in our process perfect. We need to work to steadily improve it as we grow and expand our software. Deployment architecture is just another peice of software that   