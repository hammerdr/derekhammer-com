---
layout: post
title: "Empathy for Operations is a Developer Core Competency"
description: ""
category: 
tags: []
---

The title is the statement. I'll break it down and then try to explain my thoughts behind why I think this statement is true. And then I'll give a little bit of direction to help embark on a journey.

## Empathy for Operations

Much of what we have learned recently in software development is that could software comes for empathizing with someone else. We need to be able to step away from our own viewpoint and see it from someone else's perspective. To improve the business value of the product, we need to view it from the business owners perspective. To improve the user experience, we need to view it from the users perspective. To improve the code quality, we need to view it from the abstract Future Developer's point of view.

In order to improve how our applications run in production, we need to empathize with operations. Empathy in this case is much like those other cases. We need to put ourselves into the shows of the operations people and ask how we can make their lives easier.

Of course, this means that we need to understand a little more about what operations people do. This means we should involve the operations people a little more than we do. This means we should consider them when making large decisions that impact them. We should ask questions and listen to the answers. Often we have little idea of what is going on in their world.

## Developer Core Competency

Core competencies is the idea that these are the skills, techniques and values that are needed in order to be a successful developer. These competencies are multifaceted and developers are most likely to be stronger in one area or another.

Examples of a developer core competency are debugging, unit testing, parsing files, basic language concepts such as object orientation or functional programming, a database or two, estimation, a particular language or two, and tasking. I've seen programmers succeed without a core competency or two but things start to get ugly quickly if those are not rectified quickly. And we entire teams or companies of teams lack a core competency, it is a train wreck waiting to happen.

It is good to identify the core competencies not as a way to judge others but as a way of creating a checkpoint for ourselves to evaluate against. If I'm not aware of how to debug properly (even if its in a new language that I'm trying out!), I should take a stab at trying to up skill myself in that area.

## The Pitch

This probably sounds a lot like that 'DevOps' thing you keep hearing about. And it sort of is. But, I'm not asking developers to learn infrastructure as code tools or understand what Logstash is [1]. I'm asking for basic empathy for the people that will end up doing that work.

Ask yourself questions like "What does installing this new database do to my operations team?" Sometimes, the answers will be "there is no material effect on the operations side." Sometimes, the answers will be "this will put undue effort on the operations team." Most times, the answers will be in the middle. 

When developers start to ask these questions, decisions that affect development and operations get resolved quicker. We don't use a new database and throw it to the operations team, only to have them come back and tell us that they refuse to support it. Or worse, they attempt to support it but overload themselves. The decisions that would have that result get short circuited.

Best would be to have the operations team embedded in your own so that they can work to support a new product or feature from Day 1. Even better is to have the whole team responsible for the ongoing support of that product. Unsupportable decisions will 'magically' go away when everyone needs to consider the consequences of that new database. But we're venturing off into more advanced land that isn't core competency.

Developers, at the very least, need to want to understand the affect their changes have to production systems. They need to want to create database migration scripts -- both forward and backwards. They need to want to minimize unnecessary complexity in the production stack. It will go a long way toward making products have higher availability and less maintenance costs.

## The Quick Start

So, if you're a developer ready to up skill your empathy for operations, here a few tasks:

1. Talk to your operations people. Ask them to go over the deployment process and maintenance procedures for your product. Understand what it takes to support your upcoming changes.
2. Read [The Phoenix Project](http://www.amazon.com/The-Phoenix-Project-Helping-Business/dp/0988262592). It's quite good and a quick read. It tells the story from the perspective of the Operations person and its easy to get into their perspective.
3. Start to train yourself on some operations basics at [OpsSchool](http://www.opsschool.org/en/latest/)

That's it! Not too much in the way of a quick start. Just constantly ask yourself, "How can I better understand what the operations team/my own team will be needing to do to support availability and decrease maintenance costs?"

[1] Though, understanding Logstash and Infrastructure as Code tools can be a great way to start to bridge the gap between development and operations.