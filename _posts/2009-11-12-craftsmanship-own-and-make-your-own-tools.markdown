--- 
wordpress_id: 111
layout: post
title: "Craftsmanship :: Own (and make) your own tools"
wordpress_url: http://blog.derekhammer.com/?p=111
---
<p style="clear: both">Recently, Jon Fuller (my mentor from last summer) wrote about <a href="http://jonfuller.codingtomusic.com/2009/10/07/do-you-own-your-tools/" target="_blank">owning your own tools</a>. He also wrote about <a href="http://jonfuller.codingtomusic.com/2009/11/11/every-team-needs-a-toolsmith/" target="_blank">how every team needs a toolsmith</a>. Jon's response comes from Robert Martin's assertion that a craftsman owns his own tools. I would argue that every craftsman needs to own and create their own tools.</p>

<h1>Own your own tools</h1>
<p style="clear: both">Every craftsman should own her own tools. Why?</p>

<ul style="clear: both">
	<li>A craftsman owns her craft. She should not be dependent upon some other entity in order to function.</li>
	<li>Her tools are highly specialized and personalized (see next section).</li>
	<li>When done correctly, tooling herself with the right tools <em>for her</em> will improve the craftsman's work in both quality and efficiency.</li>
	<li>A craftsman should hone her skills outside of her currently project. Specifically, she should work with different material (play with new languages!).</li>
</ul>
<p style="clear: both">Each of these reasons can be traced back to the <a href="http://manifesto.softwarecraftsmanship.org/" target="_blank">craftsman manifesto</a>. A craftsman is a <em>professional</em> and wants to build <em>well crafted software</em>. In order to do this, breaking from the standard mold is required. The craftsman is a member of a <em>community </em>that seek to improve software in general. She can do this by honing her skills and tooling herself with the right tools. More specifically, she can find <em>what works</em> and then shares that information with the craftsman community. And, most importantly, by steadily <em>adding value</em> through responding to change. How can a craftsman add value to the product or to the community if she is not exploring the tools of software engineering?</p>
<p style="clear: both">Tools include everything that can be specialized in some way that could improve your productivity. This includes <em>your development machine</em> and all of the software on it. This includes a place to work at home: a good solid desk to sit at and to hone your profession. This includes personal cloud services such as online backup. This includes books.</p>
<p style="clear: both"><em>"My company takes care of that</em>."</p>
<p style="clear: both">For the most part, developers are bound to their company for many of the tools that they use. Sometimes, there is nothing to be done. Sometimes, you can forego the company sponsorship and get it yourself. Sometimes, developers just <em>want </em>the company to take care of it. The point is that it is always better to have a copy of "Refactoring" by Martin Fowler in your personal collection rather than have it in the company's collection. You can use it across companies (let's face it, the software industry has high turnover) and you can use it for personal development.</p>
<p style="clear: both"><em>"Visual Studio is expensive."</em></p>
<p style="clear: both">Yes, it is. Tools are expensive. Make sure your company compensates you in some way for your professional craftsmanship.</p>

<p style="clear: both"><em>"I already have everything I need."</em><em>
</em>
<p style="clear: both">Craftsmen are always, always looking for a way to improve their work and that includes there tools.</p>

<h1>Make your own tools</h1>
<p style="clear: both">Jon argues that every team member needs a toolsmith. He lists several examples of tools that he has in some way written or used throughout a project. From my experience with Jon, I know that he was likely involved in the creation of each of those tools. This isn't because Jon is a "toolsmith." It's because Jon is a craftsman.</p>
<p style="clear: both">When I was young, I spent a lot of time on my grandfather's farm. My father thought that it was a good experience for me (he was right): it would teach me work ethic and problem solving (again, he was right). One of the things that always amazed me about my grandfather was that he was always making something. My grandfather isn't a creative guy--that is, not in the traditional sense. He wouldn't make something because he wanted to; he made something because he needed to. I can pick countless times throughout my youth where I can think of my grandfather welding, sawing, assembling something out of scrap metal in order to help him solve a problem. My grandfather wasn't a toolsmith. My grandfather was a farmer; my grandfather was a craftsman.</p>
<p style="clear: both">As software engineers, we often are so focused on the problem that we cannot see the peripherals. If, for example, we are having a difficult time testing a product, we chalk it up to "the nature of the project." Sometimes we chug along and deal with the pain. Sometimes we ignore the problem. Either way, we lose, the customers lose and the business loses.</p>
<p style="clear: both">Instead, we need to see that if we spent one day writing a little bit of "scaffolding" code, if we assemble together a tool that will solve this problem for us, then many things fall into place. The engineer can now test the product in a pleasing, efficient way. The bugs that would otherwise not have been caught would be. The customers would experience a better product. The business would have a better product to sell. Everyone wins. Just by being able to recognize development pains.</p>
<p style="clear: both">One time, we needed to move some lumber. It was a ways away and in a particularly tight spot. We couldn't drive the big tractor in there. We could drive the ATV in there. However, there was a lot of wood. How were we going to effectively transport it? I suggested to my grandfather that we make a wagon. We did. It was quick, dirty and used some pretty crappy wheels off of some cart-looking thing. We welding some metal together, bolted down a sheet of plywood and attached the wheels. Quick, dirty. It did the job. And more. Today, my grandfather still uses that tiny little cart that he and I made in a little under an hour to solve a particular problem. He found that this little wagon has been very, very useful.</p>
<p style="clear: both">My point here is that sometimes this dirty little tools turn out to be elegant solutions to a problem that often occurs. Your little ruby tool that automates user documentation or starts a build or generates code could apply to everyone one of your projects. That library could then become another tool your ever growing developer toolbox. Special little tools that are as a part of your craft as your development machine.</p>

<br class="final-break" style="clear: both" />
