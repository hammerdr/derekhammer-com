---
layout: post
title: "Feedback Loop"
published: true
categories:
---

I have just finished school. Graduation is in one week. I am really excited about that. This next week should be a flurry of hanging out with my college friends, doing one last round of stupid college stuff and relaxing. After that, my life will become a very exciting for a few weeks and then I'll settle into some sort of routine (I can only assume).

However, despite the fact that I will be leaving school, I still have a lot to learn. I will be soaking up as much knowledge as I can when I go off to Chicago to work with <a href="http://thoughtworks.com">ThoughtWorks</a>. But, I will be leaving behind a great group of peers, mentors and friends. I am going to be selfish and attempt to leverage them right before I leave.

I have created what Chad Fowler refers to as a 360 review in his book The Passionate Programmer (read it!). For anyone that has worked with me, mentored me or even just interacted with me while I've been at college, I am asking that you help me out and fill out the review.

<a href="http://www.derekhammer.com/feedback">Fill out the review!</a>

I plan to leave this up indefinitely so that anyone may give me anonymous (or not anonymous.. their choice) feedback at any time. Please remember to be constructive so that I can address my own weaknesses and help improve them! Thanks!