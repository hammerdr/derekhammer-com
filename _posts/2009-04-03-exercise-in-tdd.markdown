--- 
wordpress_id: 25
layout: post
title: Exercise in TDD
wordpress_url: http://blog.derekhammer.com/?p=25
---
<p>James Grenning has a good, old <a href="http://www.renaissancesoftware.net/blog/archives/16">blog post</a> that describes the ìPhysics of TDD.î It attempts to prove in an informal method that TDD is a good method of finding bugs. Iíve read this blog post before. It was good then; it is good now. Anyone that is on the fence about TDD (or just plain interested in TDD, in general) should read this post.</p> <!--more-->  <p>For anyone that doesnít know, James Grenning is one of the authors of the Agile Manifesto. Heís a pretty popular guy in the Agile circuit. While I agree on the very basic level with Grenningís post, I think there are some <a href="http://blog.derekhammer.com/?p=10">inherent fallacies with pure</a> TDD. It needs some sort of supplement such as ATDD or BTDD.</p>
