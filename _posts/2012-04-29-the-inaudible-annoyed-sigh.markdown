---
layout: post
title: The Inaudible Annoyed Sigh
published: true
categories:
---

In case you are unaware or are in need of a reminder, this is a technical blog about software. I write my thoughts and opinions on how to write software. Today, I am going to continue that by talking about Women in Technology.

"Not that dead horse." "It's been done a thousand times." "Why aren't you talking about something REAL like how to introduce a service layer peice by peice in a Rails application?" (I'll do it one day, I promise!) This, folks, is as real as it gets. If you thought this or anything along these lines then you're giving me the inaudible annoyed sigh.

Most of the technology industry (both men and women) will have stopped reading this article by now.

Last week, a reporter at Mother Jones wrote [an article about Brogrammers](http://www.motherjones.com/media/2012/04/silicon-valley-brogrammer-culture-sexist-sxsw) and yet another boneheaded talk by a techie that glorified misogyny. There was an appropriate level of outrage on the internet by well meaning people.

When was the last time that the internet reacted to anything with the "appropriate level of outrage"? When was the last time that a technically oriented person let anything go with a wag of the finger on Twitter and continued to work on things. Never; in fact, the opposite happens. This is so common that there is a word for it: nerdrage.

I do not encourage nerdrage as a practice but the fact that the response to these incidents in the technology industry is so lukewarm and lackluster compared to the battles of epic proportions that span decades over things like "ViM v. Emacs" disappoints me. We can generate years of conversation, debate, slide decks, point and counterpoint, etc. into two tools but we cannot feel empathy for fellow human beings that may want to one day hang out with us and create cool things.

I am disappointed.

Technologists that approach Women in Technology as a peripheral, as an annoyance, as a dead horse that continues to get beaten just do not get it. When we accept that premise, we have already lost. We start to care more about tools and lifeless objects than we do about fellow human beings. We let people create slideshows of women in bikinis, ads with girls on stripper poles, propogate the misogynist brogrammer to unhealthy levels unless someone like Tasneem Raja calls it out and makes us feel guilty until we respond with the appropriate amount of disgust and finger wagging.

This is not exclusive to women (though it is very prevalent in the women minority of the tech world); technologists tend to shut out the "soft side" of technology. Technology is created by people that work in teams which often operate collaboratively. We create technology not necessarily to scratch an itch but ultimately serve our fellow human beings in a positive way. Race,  gender, social status, nationality, etc. all suffer from this "hard side" focus in technology.

So, no, I'm not looking for a nerdrage or for a public shaming of the SXSW speaker. What I'm looking for is the absense of the annoyed sigh. I'm wanting technologists to learn and understand that being aware of antihumanism is *more* important than learning how to use Redis. We should not sigh and shy away from the uncomfortable discussions that have no right or wrong and that involve people and not tools; we should embrace them as being a part of a community and realizing that part of being a technologist and human being is to embrace those conversations.

This is a blog of a technologist and I am talking about how it is important to recognize and reject misogynism in our community.

P.S. I've been known to inaudibly sigh, as well. We can all learn.