---
layout: post
title: "Politics of Code"
published: false
categories:
---

Code is political. As writers of code, we inject our political views into the code that we write. Our views mold and shape the code--a living document--into a work of fiction that resembles our world view. I realized this when someone told me earlier this week that our politics permeate our lives whether we want it to or not.

I'll start with an analogy. We can read great works of art that survived the centuries. These fictional stories have clear political bias intertwined. Beowulf, which is pretty old, describes the heroism and absolute control of a strong, worthy King that goes off the slay monsters. Anna Karenina is wrought (intentionally) with political fervor for the upheaval of the aristocracy. The Harry Potter series has magic and wizards that are ranked by the merits of their deeds. Fictional works have the political assumptions and political bias that come with the writer.

Code is, for our purpose, a work of fiction. Code writers start with nothing. Then, they express ideas. These ideas are in a structured, mathematically-based (well, except for Perl!) language from which we cannot deviate. This is because we have two intended audiences and both need to understand what we are saying. Hopefully, I am preaching to the choir: code is a creative piece of work. Code being creative and we being humans, there is bound to be our assumptions and bias thrown into the final product.

In order to understand my analysis, we need to understand my own political bias and assumptions. I have grown up in the United States. I am an American through and through. Because of this, I have a bias toward democracy, republicanism (election of representatives), liberty from the government, inalienable rights, particular freedoms such as speech, religion and thought, etc. My bias is toward a small, limited federal government with a broad interpretation of equal rights (that is to say, I am very liberal when it comes to social issues). I am a libertarian (classical liberal).

When I write code, I dislike anything that binds me. I don't like rigid, inflexible systems. Politically, I am a firm believer in the free movement of people even across country borders. I don't like over-engineered code. Why does something like the Tax Code need to be so complicated (okay, so maybe that's less politics and more common sense)? I hate overhead. Overhead is what kills me. I will fight tooth and nail to get rid of work I think is unnecessary. In a way, this is similar to how I feel about government programs and processes. I prefer small, nimble businesses as opposed to government bureaucracy. I love TDD. The greatest part about TDD is that it creates a tiny sandbox that the developer can create within. It focuses the developer. It creates a small, bounded region in which to try new things. Similarly, James Madison said in the Federalist Papers that the United States delegates much of the power to the states because it will create new pockets of democracy from which innovation can be created. That's why I believe in a small federal government.

As well as this theory seems to work, there are a few holes in my experience. For example, when I first learned about patterns, my political stance didn't stop me from going full steam ahead and over-engineering every piece of code that I touched. So, while I think there is some truth to this theory, I think it is more of a seeping effect than a direct actor in our code. 

It is also interesting to re-evaluate whom I most admire as a programmer. For me, Kent Beck is up there. I loved XP Programming Explained. Both the content and the style were spot on for me. However, I understand that some people didn't like the book. Part of the reason could be that I came out of the book knowing that the author and I would share many of the same values. I really connected to that book and part of it was the pathos of the writing.

