---
layout: post
title: Freedom of Information
published: true
categories:
---

[Baruch Spinoza](https://en.wikipedia.org/wiki/Baruch_Spinoza) is one of those names that every modern human knows in their bones, their culture and their actions but know not at all. He was a great rationalist philosopher whose postulations have become so commonplace and engrained into our culture that we are not aware of them. His ideas are, in part, the foundation of the modern democracy, morality, ethics and metaphysics. Here is what Spinoza says about freedom of information and the idea that the state needs to operate in secret:

> They who can treat secretly of the affairs of a dominion have it absolutely under their authority, and, as they plot against the enemy in time of war, so do they against the citizens in time of peace. Now that this secrecy is often serviceable to a dominion, no one can deny; but that without it the said dominion cannot subsist, no one will ever prove. But, on the contrary, to entrust affairs of state absolutely to any man is quite incompatible with the maintenance of liberty; and so it is folly to choose to avoid a small loss by means of the greatest of evils. But the perpetual refrain of those who lust after absolute dominion is, that it is to the essential interest of the commonwealth that its business be secretly transacted, and other like pretences, which end in the more hateful a slavery, the more they are clothed with a show of utility.

[Source](http://www.constitution.org/bs/poltr_07.htm#029)