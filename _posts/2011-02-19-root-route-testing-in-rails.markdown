---
layout: post
title: "Root Route Testing in Rails"
published: true
categories:
---

This one took a little bit for me to figure out and I thought that I'd share the solution.

<pre>
  it "should route root to home page" do
    opts = { :controller => 'home', :action => 'index'}
    assert_recognizes opts, '/'
  end
</pre>

If you read the [testing Rails guide](http://guides.rubyonrails.org/testing.html) you'll notice that they make a brief mention of the <code>assert\_routing</code>. That code takes the hash key and composes a route based on the parameters. This doesn't necessarily work for the root, though. In order to do that, you need to compose the route hash from a relative URL. That is what <code>assert\_recognizes</code> does for us.