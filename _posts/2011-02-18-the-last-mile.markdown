---
layout: post
title: "The Last Mile"
published: true
categories:
---

The last mile of a marathon is always the most difficult. You've run 25 miles and you just need one more. You can see, smell the finish line. It's right there in front of you. It is the most difficult (and dangerous!) part of a marathon.

In software development, the last mile problem is a big one. You've decided on a product, designed the user interface, architected a great technical solution, executed on delivering business value and passed a testing suite that ensures quality. Now you just have to release. Releasing is the most difficult and dangerous part of software development.

We can mitigate this risk. Practice makes perfect and we need practice at delivery! Big software products do not release that often. If you're doing good, you may get a product out every six months. Twice a year is all your team gets for practice.

Instead of releasing twice a year, we can release far more frequently. Monthly, weekly or even daily releases cut the risks associated with the last mile. The most extreme of this risk mitigation is continuous delivery.

Releasing takes time. It is a non-trivial task that cannot be done in a couple of steps. In order to release regularly, we need to turn that manual delivery process into an automated pipeline. The goal is to have "button push" or continuous delivery so that delivery is so easy that it is mindless. When you reach that level of complexity, risks involved with delivery drop massively.

For most companies, this is not an easy process. This needs to be a conscious choice to reach a better state over a long period of time. You'll need someone that is assigned tasks associated with this goal that can spend significant chunks of time on delivery automation. You'll also need the rest of the IT organization to buy into process. Cultural change also takes time.

Release does not have to mean production, though. In fact, I am currently working for an organization that is attempting to shift to automated delivery and monthly releases to production. However, we can now deploy daily (and soon will deploy automatically). That means for a particular monthly production release, there may be 30 or more deployments. The pipeline currently looks like:

<pre>
Dev -> CI -> Automated QA   --/-> Staging
        \--> Exploratory QA -/
</pre>

Staging is a production environment that is not accessible. We utilize [blue/green deployments](http://martinfowler.com/bliki/BlueGreenDeployment.html) so that, while we still deploy every day, we don't necessarily have to push to production. When we are ready for a production release, we just need to point the incoming connections to Staging (which becomes Production) and then start deploying to the old Production (which becomes Staging).

For truly green field organizations (mostly just start ups), I would recommend that you release every day. Whether that means manually calling "heroku push" at the end of each day or a structured deployment pipeline, it does not really matter. Getting a culture of releasing every day is important for risk management [and other great things, too.](http://mygengo.com/talk/blog/release-schedules/) Release early, release often and remove the pain with automated deployments!