---
layout: post
title: One Year Goals
published: true
categories:
---

Several people state that defining long term goals is a good thing. I tend to believe them and so I'm going to create a list of long term goals. By April Fools' Day, 2011, I want to achieve the following goals. These goals may be out of reach but hopefully by reaching a little I can actually surprise myself and meet my goals!

# Professional

I am going to start off with professional goals. This is the start of my professional life and I am very excited. I feel that I am starting in a great place; I am beginning life at a great company, I have a wonderful support network of colleagues, mentors, mentees and friends in the professional world and I am starting a "green field career" where I am carrying no baggage of bad experiences. However, I do not want to be satisfied with this. I need to set some goals that should help me launch into further success.

## 1. Find another mentor

I already have a few great mentors in Jon Fuller, Curt Clifton and Sriram Mohan. While I will continue to lean on my existing mentors, I would like to find another mentor in the coming year. The mentor/mentee relationship is one of the strongest in the professional world and having great mentors make you that much better. It would be a bonus to find a mentor in Thoughtworks (there should be plenty of viable candidates!) that would have experiences and advice relating to the company.

## 2. Produce for mentees

A mentor is often something that is intangible. She is there as a guide and a sounding board. However, there are also some tangibles that come from mentors. While I'm not particular about the tangible, I do want to produce something of the nature for Pete Brousalis and Eric Stokes. It could be a recommendation to a particular job offer or something else that has a direct impact on their lives.

## 3. Put an idea into action

I have a ton of ideas. Unfortunately, not many of them see the light of day. I want to put one idea into action. It could be as simple as writing and maintaining an open source project that I've been thinking about or launching a small business venture. I want to do something positive with my creativity.

## 4. Stay disciplined

I am very scatterbrained. I will do something for a couple of weeks and then lose interest. One great example is doing katas. I had done them for a few weeks but then lost track of doing them due to "time constraints." I need to stay disciplined and maintain my schedule in a professional manner.

## 5. Learn

This summer, I will be starting work at Thoughtworks. I will be working with some of the best minds in software development and I need to put that opportunity to use. I need to stay humble and try to soak up as much knowledge as possible. If necessary, I should keep a journal of the things that I am learning so that I do not miss the opportunity to internalize the wisdom.

## 6. Speak at a conference

Public speaking is a learned art. In order to get better at it, I need to practice. I should take every opportunity that I can to speak publicly--whether it is at local interest group meetings or even small presentations to project groups. Ultimately, though, I want to give a speech at a conference of some significance--it can be a regional or domain specific conference.

## 7. Mentor someone under 18

I think that mentoring is a key element of our responsibilities as professionals. I want to get involved with someone young (the younger the better, really) that has an interest in computing. It would be fantastic if I could find a young woman to mentor. I have no idea how I am going to accomplish this but hopefully I can pull it off!

# Personal

As well as starting my professional life, I will be starting a new section of my personal life. I will be moving to Chicago (my first real city) and living in my own apartment (for the first time on my own) with almost no one around that I know. It'll be a great experience and I am looking forward to it (though I am a little terrified). My personal goals are probably a bit more humble than my professional goals.

## 8. Make non-work friends

I love what I do. However, I cannot put my entire life into my work. I need to make some friends outside of work that I can hang out with when I need to get away. I have been able to do this in the past so I am not worried but listing this as a goal helps me avoid the trap of just work friends.

<div class="image_with_caption_no_maxes on_the_right">
  <img src="/images/goals.png" alt="Goals" />
</div>

## 9. Reduce clutter

Clutter is around everyone. I have a ton of gizmos, gadgets, electronics, souvenirs, clothes, etc. that I've collected. I'm not generally a packrat but I want to make my life simpler. I need to start getting rid of stuff that I once though were essential. If I can get rid of my TV (and HTPC that goes with it), that will be a huge win.

## 10. Cross-Train

In sports, athletes are told that they need to cross-train in order to be the most effective. Cross training is the act of training in a sport that is not your primary. This training has proven to make the athletes better at their primary sport. In that sense, I need to find something that is intellectually stimulating that is not computing. Philosophy may be something that I pick up.

## 11. Live healthier

This one is going to be tough. In college, we all tend to abuse our bodies. I need to put my college lifestyle behind me and live healthier. This probably means eating home cooked meals and exercising more.

# Big Poster

In order to remind myself of this, I've actually taken an idea from software development. I am going to create a big poster and hang it on the wall. This poster just lists the 11 goals that I made above. Hopefully, it will be a constant reminder of my long terms goals and I can stay focused on achieving them.