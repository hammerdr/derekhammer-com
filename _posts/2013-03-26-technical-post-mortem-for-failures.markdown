---
layout: post
title: Technical Post-Mortem for Failures
published: true
categories:
---

This is just a quick template that I am using with my teams.

# Hammer's Technical Post-Mortem for Failures

1. What was the problem? Please be as concrete and simple as possible (e.g. the build failed due to data not being cleaned up the database)
2. What was the root cause of the problem? Consider using the 5 Whys in order to really track down root causes.
3. Was the issue resolved quickly? If so, what processes or techniques made that happen. If not, what gaps in our processes or techniques exist?
4. Identify actionable items that would resolve the root cause of the issue or change the processes and techniques to accommodate failures of this type.
5. Assign owners to the action items.
