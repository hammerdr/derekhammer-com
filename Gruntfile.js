
module.exports = function(grunt) {
  grunt.initConfig({
    rsync: {
      options: {
        args: ["--verbose"],
        ssh: true,
        recursive: true
      },
      prod: {
        options: {
          src: "_site/",
          dest: "~/public_html/",
          host: "derekham@derekhammer.com",
          syncDestIgnoreExcl: true
        }
      }
    },

    sass:{
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          '_site/css/pygments.css' : '_css/pygments.scss',
          '_site/css/screen.css'   : '_css/screen.scss'
        }
      }
    },
    watch: {
      style: {
        files: ['_css/*.scss'],
        tasks: ['create-site'],
        options: {
          interrupt: true,
        },
      },
      html: {
        files: ['partials/*.html', '_layouts/*.html', '_includes/**/*.html'],
        tasks: ['create-site'],
        options: {
          interrupt: true,
        },
      }
    },
    clean: {
      jekyll: ['_site']
    },
    jekyll: {
      options: {
        bundleExec: true,
        src : '<%= app %>'
      },
      dist: {
        options: {
          dest: '_site',
          config: '_config.yml',
          raw : 'assetTimestamp: <%= grunt.option("time") %>'
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 4000,
          base: '_site'
        }
      }
    },

  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-rsync');
  grunt.loadNpmTasks('grunt-jekyll');

  grunt.registerTask('getTimeStamp', function(){
    grunt.option('time', new Date().getTime());
    console.log('Timestamp set: '+grunt.option('time'));
  });

  grunt.registerTask('create-site', ['getTimeStamp', 'jekyll', 'sass']);
  grunt.registerTask('build-and-watch', ['connect', 'create-site', 'watch']);
  grunt.registerTask('build', ['clean', 'create-site']);
  grunt.registerTask('publish', ['build', 'rsync']);
  grunt.registerTask('default', ['build-and-watch']);

};
